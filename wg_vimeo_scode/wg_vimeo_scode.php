<?php

/**
 *
 * @link              http://webigear.com
 * @since             1.0.0
 * @package           wg_vimeo_scode
 *
 * @wordpress-plugin
 * Plugin Name:       WG Custom Vimeo Player
 * Plugin URI:        http://fiverr.com/webigear
 * Description:       Custom Vimeo ShortCode Plugin by <a href="http://fiverr.com/webigear">Webigear Digital</a>. Use Shoortcode [wg_vimeo_scode] to display vimeo without controls. Enable controls with [wg_vimeo_scode controls=1]
 * Version:           1.0.0
 * Author:            Webigear Digital
 * Author URI:        http://fiverr.com/webigear
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wg_vimeo_scode
 * Domain Path:       /languages
 */

function wg_vimeo_scode($atts)
{
    extract( shortcode_atts( array(
        'url' => 'https://player.vimeo.com/video/273183390',
        'controls' => '0',
        'muted' => '1',
        'autoplay' => '1',
        'api' => '1',
        'loop' => '1',
        'width' => '100vw',
        'height' => '100vh',
        'background' => '#000',
    ), $atts ) );

    $final_url = $url
        . '?autoplay=' . $autoplay
        . '&muted=' . $muted
        . '&controls=' . $controls
        . '&loop=' . $autoplay
        . '&api=' . $api;

    ob_start();?>

    <style>
        @media only screen and (min-width:480px){
            .vimeo-video-iframe{
                width:<?php echo  $width;?>;
                height:<?php echo  $height?>;
            }
        }
        .vimeo-video-iframe{
            min-height:600px;
            max-width:100%;
            max-height:100%;
            background-color:<?php echo $background;?>
        }
        @media only screen and (max-width:480px){
            .vimeo-video-iframe{
                min-height:300px;
            }
        }
        .vimeo_container{
            overflow:hidden;
        }
        #sound {
            top: 0;
            position: absolute;
            z-index: 999999;
            left: 0;
            background: black;
            padding: 5px 20px;
            color: white;
            border-radius: 10px;
            font-size: 20px;
            cursor:pointer;
        }

    </style>

    <script src="https://player.vimeo.com/api/player.js"></script>

    <script>

        jQuery(document).ready(function(){
            var iframe = jQuery('.vimeo-video-iframe')[0];
            var player = new Vimeo.Player(iframe);

            jQuery('#sound').on('click', function () {

                if (jQuery(this).hasClass('mute')) {
                    jQuery(this).removeClass('mute').html('<i class="fas fa-volume-up"></i>');
                    player.setMuted(false);
                    player.setVolume(1);
                } else {
                    jQuery(this).addClass('mute').html('<i class="fas fa-volume-off"></i>');
                    player.setVolume(0);
                }
            });
            

        });

    </script>

    <div class="vimeo_container">
        <div id="sound" class="ui black big launch right attached fixed button mute"><i class="fas fa-volume-off"></i></div>
        <iframe width="100%" height="100%" class="vimeo-video-iframe" allowfullscreen="" allow="autoplay" title="vimeo Video Player" src="<?php echo $final_url ?>"></iframe>
    </div>

    <?php

    $output_string = ob_get_contents();
    ob_end_clean();

    return $output_string;

}

add_shortcode('wg_vimeo_scode', wg_vimeo_scode);
